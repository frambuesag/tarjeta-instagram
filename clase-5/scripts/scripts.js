/**
 * Escuchar cuando el usuario haga click en 'Publicar' ✔️
 * Ir a traer el input ✔️
 * Tomar el valor que el usuario ingreso en el input ✔️
 * Ir a traer el contenedor de los comentarios  ✔️
 * Agregar el valor del usuario en el contenedor ✔️
 */


// Escuchar cuando el usuario haga click en 'Publicar'
const postButton = document.querySelector('#post-comment');
const commentInput = document.querySelector('#comment-input');
const commentFeed = document.querySelector('#comment-feed');

postButton.addEventListener('click', function() {
    const userInput = commentInput.value;
    const newComment = `
        <div class="comment">
            <div class="comment-img-container">
                <img class="user-avatar" src="https://scontent-mia3-2.cdninstagram.com/v/t51.2885-19/s150x150/22582088_735399073336517_7044282068120895488_n.jpg?_nc_ht=scontent-mia3-2.cdninstagram.com&_nc_ohc=Ws7h4whnUQgAX_p0RMd&oh=d3307fe00d1fe04244523abbf2e8adea&oe=5E897729" alt="colossal">
            </div>

            <div class="comment-info-container">
                <p class="comment-content">
                    <a href="#">
                        <b>colossal</b>
                    </a>
                    ${userInput}
                    <button class="comment-like">
                        <i class="far fa-heart"></i>
                    </button>
                </p>
            </div>
        </div><!-- comment -->
    `;

    commentFeed.innerHTML = commentFeed.innerHTML + newComment;
    commentInput.value = '';
});

//Agregar funcionalidad al boton like
const buttons = document.querySelectorAll('.comment-like');

function addEventToButtons() {
    buttons.forEach(function(currentButton) {
        currentButton.addEventListener('click' , function() {
            currentButton.style.color = 'red';
    
    
            const icon = currentButton.querySelector('i');

           if (icon.classList.contains('fas')) {
               icon.classList.replace('fas' , 'far');
           } else {
               icon.classList.replace ('far' , 'fas');
           }
        })
    })    
}
